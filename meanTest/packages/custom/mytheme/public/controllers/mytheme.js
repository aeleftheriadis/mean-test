'use strict';

/* jshint -W098 */
angular.module('mean.mytheme',['mean.system','ui.bootstrap','ui.router']).controller('MythemeController', ['$scope', '$window','Global', 'Mytheme','$http','$filter','$location',
  function($scope,$window, Global, Mytheme, $http,$filter,$location) {
    $scope.global = Global;
    $scope.package = {
      name: 'mytheme'
    };
    // Function to return airports asynchronously
    $scope.getAirport = function (val) {
      console.log(val);
      return $http.get ('/api/airports' , {
        params : {
          airport : val
        }
      }).then (function (response) {
        console.log(response);
        return response.data.map (function (item) {
          return item;
        });
      });
    };

    $scope.airports = ["AAR","ABZ","ABJ","AUH","ACC","ACA","ADD","ADL","ADE","AGA","AJA","CAK","ALB","ABQ","AHO","ALG","AHU","ALC","ASP","LEI","AMM","AMS","ANC","ANK","ESB","AAE","ANU","ANR","ARI","AUA","ASM","ASU","ATH","ATL","AKL","BWN","BGW","BAG","BAH","BWI","BKO","BLR","BKK","BGF","BDJ","BJL","BGI","BCN","BRI","BRR","BAQ","BSL","BIA","BVA","BEW","BEY","BEL","BFS","BEG","BHZ","BEB","BEN","BGO","BER","TXL","THF","BDA","BRN","BIQ","BIO","BHX","BHM","BXO","BLZ","BOO","BOG","BLQ","BOD","BOS","BOH","BSB","BTS","BZV","BRE","BES","BDS","BNE","BRS","BRU","BUH","BUD","BUE","AEP","EZE","BUF","BJM","BUQ","CAG","CNS","CAI","CCU","YYC","CLO","CLY","CAL","CBR","CUN","CAN","CPT","CCS","CWL","CAS","CMN","CTA","CAY","CLT","CNX","CHI","CGX","MDW","ORD","CHC","CVG","CFE","CLE","BKL","CMR","CGN","CMB","CMH","CKY","CND","CZL","CPH","CFU","ORK","COO","CUR","DAC","DKR","DAL","DAM","DAR","DRW","DAY","DEL","DPS","DEN","DTT","DTW","YIP","DHA","DNR","DJE","JIB","DOH","DLA","DXB","DUB","DBV","DUD","DND","DUR","DUS","EMA","EDI","YEG","EIN","ELP","EBB","ECN","EBJ","EXT","FAE","FAI","FAO","FEZ","FNT","FLR","FRL","FDF","FWA","FRW","FRA","FPO","FNA","FUK","FNC","GAE","GRG","GVA","GOA","GEO","GIB","GIS","GLA","PIK","GOM","GOT","GRZ","GNB","GDL","GUM","GRU","GUA","GYE","GCI","YHZ","HAM","HLZ","HAJ","HRE","BDL","HAV","HEL","HER","HIJ","HBA","SGN","HKG","HNL","HOU","IAH","IBZ","IGU","IND","INN","INV","IFN","ISB","IOM","IST","IVL","IZM","JAX","CGK","JED","JER","JRS","JNB","JKG","KBL","KOJ","KAN","MKC","KHI","KHH","KTM","KEM","KRT","IEV","JLJ","KGL","KIN","FIH","KKN","KOI","KLU","KOA","KRS","KSU","KUL","KCH","KWI","LCG","LOS","LHE","LKL","LAN","LPB","LCA","LPA","LAS","LST","LBA","LEJ","LTQ","LBV","LGG","LIL","LIM","LNZ","LIS","LPL","LVI","LJU","LFW","LON","LGW","LHR","STN","LGB","LRT","LAX","BUR","SDF","LDE","LAD","FBM","LLA","LUN","LTN","LUX","LYS","MKY","MAA","MAD","SEZ","MAH","AGP","MMA","MLA","MGA","MAO","MAN","MNL","MPM","MAR","RAK","MRS","MBH","MRU","MES","MDE","MED","MEL","MEM","MID","MZM","MEX","MIA","MIL","LIN","MXP","MKE","MSP","MGQ","MQN","MBA","MIR","MLW","ROB","MBJ","MTY","MVD","MPL","YUL","YMX","MOW","DME","SVO","VKO","MON","ISA","MLH","BOM","MUC","MSR","MCT","NAN","NGS","NGO","NBO","OSY","NTE","NAP","NAS","NDJ","NLA","NSN","NCL","MSY","NYC","JFK","LGA","EWR","NIM","NCE","FNI","NRK","NWI","PTE","NKC","NOU","GEA","NUE","OKA","OKC","OLB","MCO","OMA","OPO","ORN","OSA","OSL","OST","OSD","YOW","OUA","OUD","PPG","PLM","PMO","PSP","PMI","PTY","PPT","PBM","PAR","CDG","ORY","PEK","PEN","PGF","PER","PSR","PHL","PNH","PHX","PSA","PIT","PLY","PNR","PTF","PIF","POA","PAP","PLZ","POG","PHE","PDX","POM","POS","PZU","VLI","PRG","PPP","PVD","PUS","YQB","ZQN","UIP","UIO","RAB","RBA","RGN","RKT","REC","REG","RNS","REK","KEF","RHO","RMI","RIO","GIG","SDU","RUH","ROK","ROM","RTM","SCN","EBU","SKB","STL","SLU","LED","SLC","SZG","SAH","SAT","SAN","SFO","SJO","SJU","SIG","SAP","SAL","SMA","SDR","SCL","SCQ","SDQ","SAO","CHG","VCP","SPK","SEL","SEZ","SVQ","SHA","SNN","SHJ","SIN","SKP","SOF","SOU","SEN","SPU","STN","SVG","STO","ARN","BMA","SYY","SXB","STR","SUB","SUV","SYD","SYR","TPE","TLL","TPA","TMP","TNR","TNG","TAS","MME","THR","TLV","TCI","TTU","SKG","TIA","TGD","TAB","TYO","HND","NRT","YYZ","TLS","TUF","TRS","TIP","TOS","TRD","TUL","TUN","TRN","TKU","UPG","VAA","VLC","YVR","VAR","VCE","VRN","VHY","VIE","VGO","VBY","WAW","WAS","IAD","DCA","WLG","PBI","WDH","YOG","YWG","YAO","YNG","ZAG","ZAH","ZNZ","ZRH"];


    //Init number of passenger
    $scope.passenger = {
      min: 1,
      max: 10,
      value: 1
    };

    $scope.today = function() {
      $scope.departureDate = new Date();
    };
    $scope.today();

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $scope.status.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','yyyy-MM-dd'];
    $scope.format = $scope.formats[0];

    $scope.status = {
      opened: false
    };

    $scope.result = [];
    $scope.submit = function() {
      $scope.result == [];
      if ($scope.originAirport) {
        $scope.result.push(this.originAirport);
      }
      if ($scope.destinationAirport) {
        $scope.result.push(this.destinationAirport);
      }

      if ($scope.departureDate) {
        var _date = $filter('date')(new Date(this.departureDate), $scope.formats[4]);
        $scope.result.push(_date);
      }
      if ($scope.passenger.value) {
        if($scope.passenger.value>1){
          $scope.result.push(this.passenger.value+ "passengers");
        }
        else{
          $scope.result.push(this.passenger.value+ "passenger");
        }

      }

      console.log($scope.result);
      var url = "http://" + $window.location.host + '/flight-search-page/' + $scope.result[0]+"-"+ $scope.result[1]+"/"+$scope.result[2]+"/"+$scope.result[3];
      console.log(url);
      $window.location.href = url;
    };
  }
]).config(['$viewPathProvider', function($viewPathProvider) {
    $viewPathProvider.override('system/views/index.html', 'mytheme/views/index.html');
}]).directive('passenger', function(){
    return    {
      restrict:'E',
      scope:{
        min: '=',
        max:'=',
        value:'='
      },
      template: '<div class="input-group">' +
      '<span class="input-group-btn"><button class="btn btn-info" type="button" ng-click="value = value - 1" ng-disabled="value <= min">-</button></span>' +
      '<input type="text" class="form-control" value="{{value}}" required>' +
      '<span class="input-group-btn"><button class="btn btn-info" type="button" ng-click="value = value + 1" ng-disabled="value >= max">+</button></span>' +
      '</div>'

    }
  }).controller('MythemeResultController', ['$scope', 'Global', 'Mytheme','$http','$filter','$stateParams',
    function($scope, Global, Mytheme, $http,$filter,$stateParams) {
      $scope.global = Global;
      $scope.package = {
        name : 'mytheme'
      };

      $scope.FromTo = $stateParams.FromTo;
      $scope.Date = $stateParams.Date;
      $scope.Passengers = $stateParams.Passengers;



      $http({
        method: 'POST',
        url: '/api/flight-search-result',
        data: '',
        processData: false,
        responseType: "json",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).success(function(data) {
        console.log(data);
        $scope.result = data;
      });
    }
  ]).config(['$viewPathProvider', function($viewPathProvider) {
      $viewPathProvider.override('system/views/result.html', 'mytheme/views/result.html');
}]);