'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Mytheme = new Module('mytheme');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Mytheme.register(function(system, app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Mytheme.routes(app, auth, database);


  Mytheme.aggregateAsset('css', 'mytheme.css');
  Mytheme.aggregateAsset('css','../lib/bootswatch-dist/css/bootstrap.css',{global:true,  weight: 1, group: 'header'});
  Mytheme.angularDependencies(['mean.system']);

  app.set('views', __dirname + '/server/views');


  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Mytheme.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Mytheme.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Mytheme.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Mytheme;
});
