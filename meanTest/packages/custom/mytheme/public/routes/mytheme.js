'use strict';

angular.module('mean.mytheme').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('Home', {
      url: '/',
      templateUrl: 'mytheme/views/index.html'
    }),
    $stateProvider.state('Flight Search Page', {
      url: '/flight-search-page/{FromTo}/{Date}/{Passengers}',
      templateUrl: 'mytheme/views/result.html'
    });
  }
]);

