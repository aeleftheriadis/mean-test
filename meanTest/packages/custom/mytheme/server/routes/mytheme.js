'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Mytheme, app, auth, database) {

  var myTheme = require('../controllers/mytheme')(Mytheme);
      // _ = require('lodash');

  app.route('/api/flight-search-result')
    .post(myTheme.flightSearch);


  //app.get('/api/airports', function(req, res, data) {
  //  var airports = ["AAR","ABZ","ABJ","AUH","ACC","ACA","ADD","ADL","ADE","AGA","AJA","CAK","ALB","ABQ","AHO","ALG","AHU","ALC","ASP","LEI","AMM","AMS","ANC","ANK","ESB","AAE","ANU","ANR","ARI","AUA","ASM","ASU","ATH","ATL","AKL","BWN","BGW","BAG","BAH","BWI","BKO","BLR","BKK","BGF","BDJ","BJL","BGI","BCN","BRI","BRR","BAQ","BSL","BIA","BVA","BEW","BEY","BEL","BFS","BEG","BHZ","BEB","BEN","BGO","BER","TXL","THF","BDA","BRN","BIQ","BIO","BHX","BHM","BXO","BLZ","BOO","BOG","BLQ","BOD","BOS","BOH","BSB","BTS","BZV","BRE","BES","BDS","BNE","BRS","BRU","BUH","BUD","BUE","AEP","EZE","BUF","BJM","BUQ","CAG","CNS","CAI","CCU","YYC","CLO","CLY","CAL","CBR","CUN","CAN","CPT","CCS","CWL","CAS","CMN","CTA","CAY","CLT","CNX","CHI","CGX","MDW","ORD","CHC","CVG","CFE","CLE","BKL","CMR","CGN","CMB","CMH","CKY","CND","CZL","CPH","CFU","ORK","COO","CUR","DAC","DKR","DAL","DAM","DAR","DRW","DAY","DEL","DPS","DEN","DTT","DTW","YIP","DHA","DNR","DJE","JIB","DOH","DLA","DXB","DUB","DBV","DUD","DND","DUR","DUS","EMA","EDI","YEG","EIN","ELP","EBB","ECN","EBJ","EXT","FAE","FAI","FAO","FEZ","FNT","FLR","FRL","FDF","FWA","FRW","FRA","FPO","FNA","FUK","FNC","GAE","GRG","GVA","GOA","GEO","GIB","GIS","GLA","PIK","GOM","GOT","GRZ","GNB","GDL","GUM","GRU","GUA","GYE","GCI","YHZ","HAM","HLZ","HAJ","HRE","BDL","HAV","HEL","HER","HIJ","HBA","SGN","HKG","HNL","HOU","IAH","IBZ","IGU","IND","INN","INV","IFN","ISB","IOM","IST","IVL","IZM","JAX","CGK","JED","JER","JRS","JNB","JKG","KBL","KOJ","KAN","MKC","KHI","KHH","KTM","KEM","KRT","IEV","JLJ","KGL","KIN","FIH","KKN","KOI","KLU","KOA","KRS","KSU","KUL","KCH","KWI","LCG","LOS","LHE","LKL","LAN","LPB","LCA","LPA","LAS","LST","LBA","LEJ","LTQ","LBV","LGG","LIL","LIM","LNZ","LIS","LPL","LVI","LJU","LFW","LON","LGW","LHR","STN","LGB","LRT","LAX","BUR","SDF","LDE","LAD","FBM","LLA","LUN","LTN","LUX","LYS","MKY","MAA","MAD","SEZ","MAH","AGP","MMA","MLA","MGA","MAO","MAN","MNL","MPM","MAR","RAK","MRS","MBH","MRU","MES","MDE","MED","MEL","MEM","MID","MZM","MEX","MIA","MIL","LIN","MXP","MKE","MSP","MGQ","MQN","MBA","MIR","MLW","ROB","MBJ","MTY","MVD","MPL","YUL","YMX","MOW","DME","SVO","VKO","MON","ISA","MLH","BOM","MUC","MSR","MCT","NAN","NGS","NGO","NBO","OSY","NTE","NAP","NAS","NDJ","NLA","NSN","NCL","MSY","NYC","JFK","LGA","EWR","NIM","NCE","FNI","NRK","NWI","PTE","NKC","NOU","GEA","NUE","OKA","OKC","OLB","MCO","OMA","OPO","ORN","OSA","OSL","OST","OSD","YOW","OUA","OUD","PPG","PLM","PMO","PSP","PMI","PTY","PPT","PBM","PAR","CDG","ORY","PEK","PEN","PGF","PER","PSR","PHL","PNH","PHX","PSA","PIT","PLY","PNR","PTF","PIF","POA","PAP","PLZ","POG","PHE","PDX","POM","POS","PZU","VLI","PRG","PPP","PVD","PUS","YQB","ZQN","UIP","UIO","RAB","RBA","RGN","RKT","REC","REG","RNS","REK","KEF","RHO","RMI","RIO","GIG","SDU","RUH","ROK","ROM","RTM","SCN","EBU","SKB","STL","SLU","LED","SLC","SZG","SAH","SAT","SAN","SFO","SJO","SJU","SIG","SAP","SAL","SMA","SDR","SCL","SCQ","SDQ","SAO","CHG","VCP","SPK","SEL","SEZ","SVQ","SHA","SNN","SHJ","SIN","SKP","SOF","SOU","SEN","SPU","STN","SVG","STO","ARN","BMA","SYY","SXB","STR","SUB","SUV","SYD","SYR","TPE","TLL","TPA","TMP","TNR","TNG","TAS","MME","THR","TLV","TCI","TTU","SKG","TIA","TGD","TAB","TYO","HND","NRT","YYZ","TLS","TUF","TRS","TIP","TOS","TRD","TUL","TUN","TRN","TKU","UPG","VAA","VLC","YVR","VAR","VCE","VRN","VHY","VIE","VGO","VBY","WAW","WAS","IAD","DCA","WLG","PBI","WDH","YOG","YWG","YAO","YNG","ZAG","ZAH","ZNZ","ZRH"];
  //  console.log(data);
  //  var patt = /data/i;
  //
  //  _.filter(airports, function(title){ return !patt.test(title);   });
  //
  //  res.json(200, airports);
  //});



  app.get('/mytheme/example/render', function(req, res, next) {
    Mytheme.render('index', {
      package: 'mytheme'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });

};

